# App Pipe Bastion Host

### Introduction

---

In this project, an application deployment pipeline will be created with scenarios for producing images with Docker and creating deployments in a Kubernetes cluster in the cloud using Secrets, Load Balancer, Cloud GCP.



### Tools

---

* Docker / Dockerfile
* Kubernetes
* Cloud GCP
* Powershell
* Secrets



### Application Architecture

---

![architecture](architecture.png)

