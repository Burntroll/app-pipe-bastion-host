#!/bin/bash

echo "Executing deployment..."

kubectl apply -f secrets.yml
kubectl apply -f load-balancer.yml
kubectl apply -f mysql-deployment.yml --record
kubectl apply -f app-deployment.yml --record
